import json
import os

from django.core.files.storage import FileSystemStorage
from django.http import HttpResponse, Http404, JsonResponse
from django.shortcuts import render
from django.views.decorators.csrf import csrf_exempt
from django.views.decorators.http import require_GET, require_POST

from django_pdf_generator import settings
from .forms import AddHtmlFileForm, AddUrlForm
from .helpers import convert


# VIEWS
def get_index(request):
    return render(request, 'index.html')


# REST API
@require_POST
@csrf_exempt
def convert_url_to_pdf(request):
    """
    Fetch url from request save generate pdf from it and return links
    for downloading and viewing this file.
    """
    data = json.loads(request.body.decode('utf-8'))
    form = AddUrlForm(data)
    if form.is_valid():
        url = form.cleaned_data['url']
        cleaned_file_name = url.split('/', maxsplit=2)[2].replace('.', '_').replace('/', '_')
        response_dict = convert(filename=cleaned_file_name, file_path=url, convert_from_file=False)
        return JsonResponse(response_dict)
    return JsonResponse({'success': False, 'details': "Not valid data."})


@require_POST
@csrf_exempt
def convert_file_to_pdf(request):
    """
    Fetch file from request save it locally, generate pdf from it and return links
    for downloading and viewing this file. Also remove uploaded file after converting.
    """
    form = AddHtmlFileForm(request.POST, request.FILES)
    if form.is_valid() and request.FILES['file']:
        uploaded_file = request.FILES['file']

        fs = FileSystemStorage()
        filename = fs.save(uploaded_file.name, uploaded_file)
        file_path = os.path.join(settings.MEDIA_ROOT, fs.url(filename))

        cleaned_file_name = filename.rsplit('.', maxsplit=1)[0].replace('.', '_')
        response_dict = convert(filename=cleaned_file_name, file_path=file_path)
        fs.delete(filename)        # remove uploaded file
        return JsonResponse(response_dict)
    return JsonResponse({'success': False, 'details': "Not valid data."})


@require_GET
def download(request, file_name: str, content_type: str):
    """
    Endpoint for downloading and viewing pdf files.
    """
    file_path = os.path.join(settings.MEDIA_ROOT, f"{file_name}.pdf")
    if os.path.exists(file_path) and content_type in ('pdf', 'force-download'):
        with open(file_path, 'rb') as fh:
            response = HttpResponse(fh.read(), content_type=f"application/{content_type}")
            response['Content-Disposition'] = 'inline; filename=' + os.path.basename(file_path)
            return response
    raise Http404


# Error Handlers
def handler404(request):
    return render(request, 'error.html', status=404, context={"code": 404})


def handler500(request):
    return render(request, 'error.html', status=500, context={"code": 500})
