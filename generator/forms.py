from django import forms
from.helpers import validate_file_extension


class AddUrlForm(forms.Form):
    url = forms.URLField(max_length=100)


class AddHtmlFileForm(forms.Form):
    file = forms.FileField(validators=[validate_file_extension])

