from celery import task
from django.core.files.storage import FileSystemStorage


@task
def remove_file(filename):
    filename = filename.split('/')[1]
    fs = FileSystemStorage()
    fs.delete(filename)
    return f"File {filename} was deleted."
