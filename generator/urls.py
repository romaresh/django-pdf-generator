from django.urls import path

from . import views

urlpatterns = [
    path('', views.get_index),
    path('convert_url/', views.convert_url_to_pdf),
    path('convert_file/', views.convert_file_to_pdf),
    path('download/<str:file_name>/<str:content_type>/', views.download),
]
