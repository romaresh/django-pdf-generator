import os
from datetime import timedelta

import pdfkit
from django.core.exceptions import ValidationError
from django.utils import timezone

from django_pdf_generator import settings
from . import tasks


def generate_links(filename: str) -> tuple:
    base = f"download/{filename}/"
    view_link = f"{base}pdf"
    download_link = f"{base}force-download"
    return view_link, download_link


def validate_file_extension(value: str):
    ext = os.path.splitext(value.name)[1]
    valid_extensions = ['.html']
    if not ext.lower() in valid_extensions:
        raise ValidationError(u'Unsupported file extension.')


def convert(filename: str, file_path: str, convert_from_file: bool = True) -> dict:
    out_file_name = f"{settings.MEDIA_ROOT}{filename}.pdf"
    try:
        if convert_from_file:
            pdfkit.from_file(file_path, out_file_name)
        else:
            pdfkit.from_url(file_path, out_file_name)
        execute_at = timezone.now() + timedelta(minutes=settings.FILE_STORAGE_TIME)
        tasks.remove_file.apply_async((out_file_name,), eta=execute_at)
    except OSError as ex:
        ex = str(ex).rsplit(':', maxsplit=1)[1].strip()
        return {'success': False, 'details': ex}
    else:
        view_link, download_link = generate_links(filename=filename)
        return {'success': True, 'view_link': view_link, 'download_link': download_link}
