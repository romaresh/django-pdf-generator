# django-pdf-generator

Test task for Admitad.
Service for generating pdf from html pages.

## Features

- For Django 2.1 and Python 3.7
- Modern virtual environments with [pipenv](https://github.com/pypa/pipenv)
- Celery + [RabbitMQ](https://www.vultr.com/docs/how-to-install-rabbitmq-on-ubuntu-16-04-47) for background removing files
- [wkhtmltopdf](https://wkhtmltopdf.org/) for converting html to pdf 

## Installation of wkhtmltopdf


## First-time setup

1)  Make sure Python 3.7x, RabbitMQ, Pipenv are already installed.

2) Install wkhtmltopdf

* Debian/Ubuntu:
```
$ sudo apt-get install wkhtmltopdf
```

* macOS:
```
$ brew install caskroom/cask/wkhtmltopdf
```
    
**Warning!** Version in debian/ubuntu repos have reduced functionality (because it compiled without the wkhtmltopdf QT patches), such as adding outlines, headers, footers, TOC etc. To use this options you should install static binary from `wkhtmltopdf <http://wkhtmltopdf.org/>`_ site or you can use `this script <https://github.com/JazzCore/python-pdfkit/blob/master/travis/before-script.sh>`_.

* Windows and other options: check wkhtmltopdf `homepage <http://wkhtmltopdf.org/>`_ for binary installers

3)Clone the repo and configure the virtualenv:

```
$ git clone https://romaresh@bitbucket.org/romaresh/django-pdf-generator.git
$ django-pdf-generator
$ pipenv install
$ pipenv shell
```

4)Create `config.json` file and put it to `calendar-api/calendar_api/settings/`. File shoud contain the following fields:

```json
{
  "SECRET_KEY": "secretkey123",
  "BROKER_URL": "rabbit url here",
  "CELERY_RESULT_BACKEND": "rabbit url here"
}

```

5)Create directory `temp_data` at project root

6).Set up the initial migration.

```
(django-pdf-generator) $ python manage.py migrate
```

7)Confirm everything is working:

```
(django-pdf-generator) $ python manage.py runserver
```
and in separate termianl start celery:
```
(django-pdf-generator) $ ./celery.sh
```

Load the site at [http://127.0.0.1:8000](http://127.0.0.1:8000).
