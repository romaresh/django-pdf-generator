from .base import *
import json
from django.core.exceptions import ImproperlyConfigured

with open('django_pdf_generator/settings/config.json') as f:
    configs = json.loads(f.read())


def get_env_var(setting, configs=configs):
    try:
        val = configs[setting]
        if val == 'True':
            val = True
        elif val == 'False':
            val = False
        return val
    except KeyError:
        error_msg = "ImproperlyConfigured: Set {0} environment variable".format(setting)
        raise ImproperlyConfigured(error_msg)


SECRET_KEY = get_env_var("SECRET_KEY")

DEBUG = True

ALLOWED_HOSTS = ['*']

INSTALLED_APPS += [
    'generator',
]

CORS_ORIGIN_ALLOW_ALL = True

FILE_STORAGE_TIME = 30    # in minutes

# Rabbit
BROKER_URL = get_env_var("BROKER_URL")
CELERY_RESULT_BACKEND = get_env_var("CELERY_RESULT_BACKEND")

