import os
from . import settings
from celery import Celery

os.environ.setdefault('DJANGO_SETTINGS_MODULE', 'django_pdf_generator.settings')
app = Celery('django_pdf_generator')

app.config_from_object('django.conf:settings')
app.autodiscover_tasks(lambda: settings.INSTALLED_APPS)



